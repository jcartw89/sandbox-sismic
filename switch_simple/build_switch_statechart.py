from sismic.io import export_to_plantuml

from sismic.model import Statechart, CompoundState, BasicState, Transition


def get_simple_switch_statechart():

    # Initialize Statechart
    statechart = Statechart(name="Switch", description=None, preamble=None)

    # Add root state
    state = CompoundState(name="root", initial="switch_off",
                          on_entry=None, on_exit=None)
    statechart.add_state(state=state, parent=None)

    # Add 'Switch Off' state
    state = BasicState(name="switch_off", on_entry=None, on_exit=None)
    statechart.add_state(state=state, parent="root")

    # Add 'Switch On' state
    state = BasicState(
        name="switch_on", on_entry='send("Turned Switch ON")', on_exit='send("Turned Switch OFF")')
    statechart.add_state(state=state, parent="root")

    # Add transition (switch_off -> switch_on)
    transition = Transition(source="switch_off", target="switch_on",
                            event="flick", guard=None, action=None, priority=None)
    statechart.add_transition(transition)

    # Add transition (switch_on -> switch_off)
    transition = Transition(source="switch_on", target="switch_off",
                            event="flick", guard=None, action=None, priority=None)
    statechart.add_transition(transition)

    return statechart


if __name__ == "__main__":
    sc = get_simple_switch_statechart()
    plantuml = export_to_plantuml(sc)
    print(plantuml)
