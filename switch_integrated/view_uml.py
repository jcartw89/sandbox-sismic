from sismic.io import import_from_yaml, export_to_plantuml

# Load statechart from yaml file
sc = import_from_yaml(filepath='./switch.yaml')

plantuml = export_to_plantuml(sc)

print(plantuml)
